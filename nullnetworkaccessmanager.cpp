#include "nullnetworkaccessmanager.h"
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>

NullNetworkAccessManager::NullNetworkAccessManager(QObject *parent) :
    QNetworkAccessManager(parent)
{
}

QNetworkReply* NullNetworkAccessManager::createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData) {
    qDebug() << "Internal: Blocked request" << op << request.url() << outgoingData;
    return QNetworkAccessManager::createRequest(op, QNetworkRequest(QUrl())); // This causes an internal error but does the job of restraining the QWebPage
}
