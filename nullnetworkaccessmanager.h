#ifndef NULLNETWORKACCESSMANAGER_H
#define NULLNETWORKACCESSMANAGER_H

#include <QNetworkAccessManager>

class NullNetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT
public:
    explicit NullNetworkAccessManager(QObject *parent = 0);

    QNetworkReply* createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData);
signals:

public slots:

};

#endif // NULLNETWORKACCESSMANAGER_H
