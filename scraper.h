#ifndef SCRAPER_H
#define SCRAPER_H

#include "scrapingfw_global.h"
#include <QObject>
#include <QList>
#include <QUrl>

class QNetworkAccessManager;
class QNetworkReply;
class QNetworkRequest;
class QWebFrame;
class QFile;

typedef QList<QNetworkAccessManager*> AccessManagerList;

class SCRAPINGFWSHARED_EXPORT Scraper: public QObject {
    Q_OBJECT
public:
    Scraper(AccessManagerList &managers_, QObject* parent = 0);
    virtual ~Scraper();

    /**
     * Call it when you want to scrap a page,
     * pass the URL you want, and you can also
     * overwrite the depth in which this url is.
     */
    void startScraping(const QString &url, int depth = 0);

    /**
     * Used to download a file to disk
     */
    bool download(const QString &url, const QString& saveFileName);

    /**
     * Get current base or full url
     */
    const QUrl& currentUrl() const;
    const QUrl& currentBaseUrl() const;

    /**
     * Abort everything
     */
    void abort();

    /**
     * Are we working?
     */
    bool isRunning() const;

    /**
     * Change/Check the maximum number of retries we can
     * do on a URL
     */
    void setMaxRetries(int num);
    int maxRetries() const;

    /**
     * Returns the depth we are at the current page
     * being parsed
     */
    int crawlerDepth() const;

    /**
     * User-Agent changing options
     */
    void setUserAgent(const QNetworkAccessManager *manager, const QString& userAgent);
    const QString& userAgent(const QNetworkAccessManager* manager) const;
    void setRandomAgent(const QNetworkAccessManager* manager);

    /**
     * Some network options
     */
    void setCacheEnabled(bool enabled);
    void setCookiesEnabled(bool enabled);
    void setPipeliningEnabled(bool enabled);

    /**
     * Get list of network managers we are using
     */
    const AccessManagerList& managers() const;

    /**
     * Get the list of requested urls
     */
    const QList<QUrl>& requestedUrlList() const;

    /**
     * Useful for the output of debug information 
     */
    static void writeToFile(const QString& fileName, const QString& text);

    /**
     * Parse the URL and return only the base URL
     */
    static QUrl baseUrl(const QUrl& url);

    /**
     * Get/Set max simultaneous requests,
     * default is 100
     */
    void setMaxSimultaneousRequests(int max);
    int maxSimultaneousRequests() const;
    
protected:
    /**
     * This function should be implemented,
     * check the example test for some guidance
     */
    virtual void scrap(const QWebFrame& mainFrame) = 0;

    /**
     * Adds or removes a url from the requested list,
     * useful when you want to repeat an already requested url
     */
    void addRequestedUrl(const QUrl& url);
    void removeRequestedUrl(const QUrl& url);

signals:
    /**
     * This is emitted when scraping if TOTALLY finished
     */
    void finished(bool ok) const;

    /**
     * This is emitted when a single page has finished scrapping.
     */
    void requestFinished(const QNetworkRequest& req, int errorCode, QString errorString) const;

private slots:
    void parse();
    void downloadData();

private:
    AccessManagerList managers_;   // List of QNetworkAccessManagers
    QList<QNetworkReply*> replies; // List of active network replies
    QList<QUrl> requestedUrls;     // List of requested urls
    QList<QUrl> urlQueue;          // Queue of URLS that are in line to be requested
    int currentManager;            // Current network manager that will be used for the next request
    int maxRetries_;               // Maximum number of retries on a URL
    QUrl currentUrl_;              // The url of the current page being parsed
    QUrl currentBaseUrl_;          // The base URL of the current page being parsed
    int crawlerDepth_;             // The depth of this page in the website tree
    QList<QString> managerUserAgent; // List of the names of each network manager
    quint16 userAgentListCount;    // Number of user agents we have
    bool cacheEnabled;             // Cache enabled?
    bool cookiesEnabled;           // Cookies enabled?
    bool pipeliningEnabled;        // http pipelining enabled?
    quint32 maxActiveReplies;      // Max active replies at once
    
    QNetworkReply* sendRequest(QNetworkRequest &req);
    void checkIfScraperFinished();
    QUrl checkAndResolveUrl(const QString& url);
    void checkAndRequestUrlsFromQueue(int depth);
};

#endif // SCRAPER_H
