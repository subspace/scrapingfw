#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QNetworkCookieJar>
#include "testuseragent.h"

static const int NumProxies = 25; // Number of open proxies
static const int BaseSocksPort = 9050; // First proxy port

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    
    // QNetworkProxy *proxies[NumProxies]; 
    // AccessManagerList managers;
    // for (int i = 0; i < NumProxies; ++i) {
    //     proxies[i] = new QNetworkProxy(QNetworkProxy::Socks5Proxy, "127.0.0.1", BaseSocksPort + i);
    //     QNetworkAccessManager* m = new QNetworkAccessManager();
    //     m->setProxy(*proxies[i]);
    //     managers.push_back(m);
    // }
    // TestUserAgent* tst = new TestUserAgent(managers);
    
    TestUserAgent* tst = new TestUserAgent(AccessManagerList() << new QNetworkAccessManager());
    tst->start();
    
    return a.exec();
}    

