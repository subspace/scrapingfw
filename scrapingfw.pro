#-------------------------------------------------
#
# Project created by QtCreator 2014-03-17T16:25:18
#
#-------------------------------------------------

QT       += webkit webkitwidgets

QT       -= gui

TARGET = scrapingfw
TEMPLATE = lib

DEFINES += SCRAPINGFW_LIBRARY

SOURCES += scraper.cpp \
    nullnetworkaccessmanager.cpp

HEADERS += scraper.h\
        scrapingfw_global.h \
    useragents.h \
    nullnetworkaccessmanager.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
