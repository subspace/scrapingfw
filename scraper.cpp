#include "scraper.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QWebPage>
#include <QWebFrame>
#include <QWebElement>
#include <QFile>
#include <QStack>
#include <QDebug>
#include "useragents.h"
#include "nullnetworkaccessmanager.h"

enum { File = QNetworkRequest::User, RetryCount, CrawlerDepth };

NullNetworkAccessManager nullManager(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Scraper::Scraper(AccessManagerList &managers, QObject* parent):
    QObject(parent),
    managers_(managers),
    currentManager(0),
    maxRetries_(3),
    crawlerDepth_(0),
    cacheEnabled(true),
    cookiesEnabled(true),
    pipeliningEnabled(true),
    maxActiveReplies(100)
{
    // Random User-Agent stuff
    for (userAgentListCount = 0; UserAgents[userAgentListCount]; ++userAgentListCount)
        ;

    // Initialize random seed
    qsrand(QTime::currentTime().msec());

    //  FIXME: !!! Create an user-agent entry for each manager !!!
    for (int i = 0; i < managers.size(); ++i) {
        managerUserAgent.push_back(QString());
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Scraper::~Scraper() {
    abort();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const AccessManagerList& Scraper::managers() const {
    return managers_;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::setUserAgent(const QNetworkAccessManager *manager, const QString &userAgent) {
    int i = managers_.indexOf(const_cast<QNetworkAccessManager*>(manager));
    managerUserAgent[i] = userAgent;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QString& Scraper::userAgent(const QNetworkAccessManager *manager) const {
    return managerUserAgent.at(managers_.indexOf(const_cast<QNetworkAccessManager*>(manager)));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::setRandomAgent(const QNetworkAccessManager *manager) {
    setUserAgent(manager, QString(UserAgents[qrand() % userAgentListCount]));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
QUrl Scraper::baseUrl(const QUrl &url) {
    QString urlString = url.toString();
    return QUrl(urlString.replace(urlString.section("/", -1), ""));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QUrl& Scraper::currentUrl() const {
    return currentUrl_;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QUrl& Scraper::currentBaseUrl() const {
    return currentBaseUrl_;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::writeToFile(const QString &fileName, const QString &text) {
    QFile f(fileName);
    f.open(QFile::Append);
    f.write(text.toLatin1());
    f.close();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::setMaxRetries(int num) {
    maxRetries_ = num;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Scraper::maxRetries() const {
    return maxRetries_;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Scraper::crawlerDepth() const {
    return crawlerDepth_;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::setCacheEnabled(bool enabled) {
    cacheEnabled = enabled;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::setCookiesEnabled(bool enabled) {
    cookiesEnabled = enabled;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::setPipeliningEnabled(bool enabled) {
    pipeliningEnabled = enabled;
}

#include <QNetworkCookieJar>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
QNetworkReply* Scraper::sendRequest(QNetworkRequest& req) {
    int i = currentManager++ % managers_.count();
    QNetworkAccessManager *manager = managers_.at(i);

    // HTTP Pipelining (faster scraping)
    req.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, pipeliningEnabled); // Fast!

    // Do we cache things?
    if (!cacheEnabled) {
        req.setAttribute(QNetworkRequest::CacheSaveControlAttribute, false);
        req.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::AlwaysNetwork);
    }

    // Are cookies enabled?
    if (!cookiesEnabled) {
        req.setAttribute(QNetworkRequest::CookieSaveControlAttribute, QNetworkRequest::Manual);
        req.setAttribute(QNetworkRequest::CookieLoadControlAttribute, QNetworkRequest::Manual);
    }

    // Set the custom user-agent string
    const QString& agent = managerUserAgent.at(i);
    req.setHeader(QNetworkRequest::UserAgentHeader, agent); // QT will set it to the default string case the agent is empty

    // Ready to go SEND!
    QNetworkReply* r = manager->get(req);

    // Connect to the proper reply signals
    if (req.attribute((QNetworkRequest::Attribute)File).isValid())
        connect(r, SIGNAL(readyRead()), SLOT(downloadData()));
    connect(r, SIGNAL(finished()), SLOT(parse()));

    replies.push_back(r);

    return r;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#if 0
static QByteArray headerName(QNetworkRequest::KnownHeaders header) {
    switch (header) {
    case QNetworkRequest::ContentTypeHeader:
        return "Content-Type";

    case QNetworkRequest::ContentLengthHeader:
        return "Content-Length";

    case QNetworkRequest::LocationHeader:
        return "Location";

    case QNetworkRequest::LastModifiedHeader:
        return "Last-Modified";

    case QNetworkRequest::CookieHeader:
        return "Cookie";

    case QNetworkRequest::SetCookieHeader:
        return "Set-Cookie";

    case QNetworkRequest::ContentDispositionHeader:
        return "Content-Disposition";

    case QNetworkRequest::UserAgentHeader:
        return "User-Agent";

    case QNetworkRequest::ServerHeader:
        return "Server";
    }

    return QByteArray();
}
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::startScraping(const QString &url, int depth) {
    QUrl parsedUrl(QUrl::fromPercentEncoding(url.toLatin1()));
    if (!parsedUrl.isValid()) {
        return;
    }
    if (parsedUrl.isRelative())
        parsedUrl = currentBaseUrl_.resolved(parsedUrl);
    if (requestedUrls.contains(parsedUrl))
        return;
    addRequestedUrl(parsedUrl);
    urlQueue.push_back(parsedUrl);
    checkAndRequestUrlsFromQueue(depth);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::checkAndRequestUrlsFromQueue(int depth) {
    int freeSlots = maxActiveReplies - replies.size();
    while (freeSlots > 0 && urlQueue.size()) {
        QUrl parsedUrl = urlQueue.at(0);
        urlQueue.pop_front();

        QNetworkRequest req(parsedUrl);
        req.setAttribute((QNetworkRequest::Attribute)RetryCount, 0);
        Q_ASSERT(depth >= 0);
        if (!depth)
            req.setAttribute((QNetworkRequest::Attribute)CrawlerDepth, crawlerDepth_);
        else
            req.setAttribute((QNetworkRequest::Attribute)CrawlerDepth, depth);
        sendRequest(req);
        freeSlots--;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool Scraper::download(const QString &url, const QString &saveFileName) {
    QUrl parsedUrl(QUrl::fromPercentEncoding(url.toLatin1()));
    if (!parsedUrl.isValid()) {
        return false;
    }

    if (parsedUrl.isRelative())
        parsedUrl = currentBaseUrl_.resolved(parsedUrl);

    if (requestedUrls.contains(parsedUrl))
        return false;
    addRequestedUrl(parsedUrl);

    QFile* f = new QFile(saveFileName);
    if (!f->open(QFile::WriteOnly))
        return false;

    QNetworkRequest req(parsedUrl);
    req.setAttribute((QNetworkRequest::Attribute)RetryCount, 0);
    req.setAttribute((QNetworkRequest::Attribute)File, qVariantFromValue((void *)f));
    req.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
    sendRequest(req);
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int Scraper::maxSimultaneousRequests() const {
    return maxActiveReplies;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::setMaxSimultaneousRequests(int max) {
    maxActiveReplies = max;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::downloadData() {
    QNetworkReply* r = qobject_cast<QNetworkReply*>(sender());
    QFile *f = (QFile *)r->request().attribute((QNetworkRequest::Attribute)File).value<void *>();
    f->write(r->readAll());
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::addRequestedUrl(const QUrl &url) {
    requestedUrls.append(url);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::removeRequestedUrl(const QUrl &url) {
    requestedUrls.removeAll(url);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::parse() {
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());

    /* This reply is over */
    reply->deleteLater();
    replies.removeAll(reply);

    QNetworkRequest request = reply->request();

    ///////////////////////////
    // Handle redirects
    ///////////////////////////
    //TODO: Handle possible infinite redirect loop
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (statusCode == 301 || // Moved Permanently
            statusCode == 302 || // Found
            statusCode == 303 || // See Other
            statusCode == 307) { // Temporary Redirect
        QString newUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString();
        //qDebug() << "Redirecting from" << request.url() << "to" << newUrl;
        request.setUrl(newUrl);
        addRequestedUrl(newUrl);
        sendRequest(request);

        return;
    }

    ///////////////////////////
    // Resend the request on any error
    ///////////////////////////
    if (reply->error() != QNetworkReply::NoError) {
        int retryCnt = request.attribute((QNetworkRequest::Attribute)RetryCount).toInt();
        if (retryCnt >= maxRetries_ && maxRetries_ >= 0) {
            emit requestFinished(request, reply->error(), reply->errorString());
            checkIfScraperFinished();
            return;
        }
        request.setAttribute((QNetworkRequest::Attribute)RetryCount, retryCnt + 1);
        sendRequest(request);
        return;
    }

    ///////////////////////////
    // File download finished
    ///////////////////////////
    QVariant var = request.attribute((QNetworkRequest::Attribute)File);
    if (var.isValid()) {
        QFile *f = (QFile *)var.value<void *>();
        f->close();
        emit requestFinished(request, 0, QString());
        checkIfScraperFinished();
        return;
    }

    ///////////////////////////
    // Setup a QWebPage for using the CSS Selector
    ///////////////////////////
    QByteArray data = reply->readAll();
    QWebPage page(this);
    page.setNetworkAccessManager(&nullManager); // No requests shall be sent from inside this page
    page.settings()->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);
    page.settings()->setAttribute(QWebSettings::AutoLoadImages, false);
    page.settings()->setAttribute(QWebSettings::JavaEnabled, false);
    page.settings()->setAttribute(QWebSettings::JavascriptEnabled, false);
    page.settings()->setAttribute(QWebSettings::PluginsEnabled, false);
    page.settings()->setAttribute(QWebSettings::LinksIncludedInFocusChain, false);
    page.settings()->setAttribute(QWebSettings::PrintElementBackgrounds, false);
    page.mainFrame()->setContent(data);
    currentUrl_ = reply->url();
    currentBaseUrl_ = Scraper::baseUrl(currentUrl_);
    crawlerDepth_ = request.attribute((QNetworkRequest::Attribute)CrawlerDepth).toInt() + 1;

    ///////////////////////////
    // Parsing starts here
    ///////////////////////////
    scrap(*page.mainFrame());

    emit requestFinished(request, 0, QString());
    checkIfScraperFinished();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool Scraper::isRunning() const {
    return !replies.isEmpty();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::abort() {
    foreach(QNetworkReply * r, replies) {
        r->abort();
        r->deleteLater();
    }
    replies.clear();
    currentManager = 0;
    crawlerDepth_ = 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Scraper::checkIfScraperFinished() {
    if (replies.isEmpty()) {
        crawlerDepth_ = 0;
        emit finished(true);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const QList<QUrl>& Scraper::requestedUrlList() const
{
    return requestedUrls;
}
