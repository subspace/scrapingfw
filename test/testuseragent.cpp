#include "testuseragent.h"
#include <QWebPage>
#include <QWebFrame>
#include <QWebElement>
#include <QWebElementCollection>
#include <QDebug>

TestUserAgent::TestUserAgent(AccessManagerList& managers, QObject *parent):
    Scraper(managers, parent)
{
}

TestUserAgent::~TestUserAgent()
{
}

void TestUserAgent::start()
{
    for (int i = 0; i < managers().size(); i++) {
        setRandomAgent(managers().at(i));
    }
    startScraping("http://www.whatsmyuseragent.com");
    stopScraper = false;
}

void TestUserAgent::scrap(const QWebFrame &main) 
{
    QWebElement root = main.documentElement();
    
    // Print the User-Agent
    QWebElement userAgent = root.findAll("span[id=\"body_lbUserAgent\"]").first();
    if (userAgent.isNull()) {
        qDebug() << "Couldn't find the User-Agent string.";
        return;
    }
    qDebug("User-Agent: %s", userAgent.toPlainText().toLatin1().data());
    
    // Print the IP address
    QWebElement ipAddress = root.findAll("span[id=\"body_lbIP\"]").first();
    if (ipAddress.isNull()) {
        qDebug() << "Couldn't find the IP address.";
        return;
    }
    qDebug("IP: %s", ipAddress.toPlainText().toLatin1().data());

    if (!stopScraper) {
        removeRequestedUrl(QUrl("http://www.whatsmyuseragent.com"));
        startScraping("http://www.whatsmyuseragent.com");
        qDebug() << "Again...";
    }
}

void TestUserAgent::stop()
{
    stopScraper = true;
}
