#ifndef _TESTUSERAGENT_H_
#define _TESTUSERAGENT_H_

#include "../scraper.h"

// Returns the User-Agent and the IP Address in use
// Useful for testing proxy lists
class TestUserAgent: public Scraper
{
    Q_OBJECT
public:
    TestUserAgent(AccessManagerList& managers, QObject* parent = 0);
    ~TestUserAgent();
                    
public slots:
    void start();
    void stop();
    
private:
    bool stopScraper;           // Indicate when to stop scraping
    
    void scrap(const QWebFrame& main);
};    
    
#endif
